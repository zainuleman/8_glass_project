package com.tekfirst.e8glassofwater.business.handlers;

import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;
import com.tekfirst.e8glassofwater.business.models.ParamFile;
import com.tekfirst.e8glassofwater.business.models.ParamString;
import com.tekfirst.e8glassofwater.utility.CommonMethods;
import com.tekfirst.e8glassofwater.utility.CommonObjects;
import com.tekfirst.e8glassofwater.utility.Constants;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;


public class CallForServer {

    private String url;
    private OnServerResultNotifier onServerResultNotifier;
    private String input = "";
    private ArrayList<ParamString> paramStrings = new ArrayList<>();
    private ArrayList<ParamFile> paramFiles = new ArrayList<>();
    private String NO_INTERNET = "No internet connection";
    private String API_BASE_URL = "http://yournewvision.co.uk/fleetly/";
    //    private String API_BASE_URL = "http://tyresol.com/fleetly/";
    private String API_BASE_URL_LOGS = "http://yournewvision.co.uk/flogs/";

    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier, ArrayList<ParamString> paramStrings, ArrayList<ParamFile> paramFiles) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
        this.paramStrings = paramStrings;
        this.paramFiles = paramFiles;
    }

    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier, String input) {
        this.url = url;
        this.input = input;
        this.onServerResultNotifier = onServerResultNotifier;
    }

    public CallForServer(String url, OnServerResultNotifier onServerResultNotifier) {
        this.url = url;
        this.onServerResultNotifier = onServerResultNotifier;
    }

    public interface OnServerResultNotifier {
        public void onServerResultNotifier(boolean isError, String response);
    }

    //Load data from server
    public void callForServerGet() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            ANRequest.GetRequestBuilder getRequestBuilder = AndroidNetworking.get(API_BASE_URL + url);
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            getRequestBuilder.setOkHttpClient(client);
            Log.d("Service_Response", url);
            getRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                    if (jsonStr != null && jsonStr.contains("Invalid Token")) {
                        CommonMethods.hideProgressDialog();
                        CommonMethods.showToast("Invalid token logging out.", Toast.LENGTH_SHORT);
//                        ((MainActivity) CommonObjects.getContext()).clearLogOutClear();
                    } else {
                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    }
                }

                @Override
                public void onError(ANError error) {
                    String err = Constants.Messages.NO_INTERNET_MESSAGE;
                    if (error.getErrorDetail() != null && error.getErrorDetail().equals("connectionError")) {
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    } else {
                        if (error.getErrorBody() != null) {
                            err = error.getErrorBody();
                        }
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    }
                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
        }
    }

    //Load data from server
    public void callForServerPost() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            ANRequest.PostRequestBuilder postRequestBuilder;
            if (url.equals("logs")) {
                postRequestBuilder = AndroidNetworking.post(API_BASE_URL_LOGS + url);
            } else {
                postRequestBuilder = AndroidNetworking.post(API_BASE_URL + url);
            }
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            postRequestBuilder.setOkHttpClient(client);
            Log.d("Service_Response", url);
//            for (ParamString paramString : paramStrings) {
//                Log.d("Service_Response",paramString.getKey()+" "+paramString.getValue());
//                postRequestBuilder.addBodyParameter(paramString.getKey(), paramString.getValue());
//            }
//            for (ParamFile paramFile : paramFiles) {
//                Log.d("Service_Response",paramFile.getKey()+" "+paramFile.getFile().getAbsolutePath());
//                postRequestBuilder.addFileBody(paramFile.getFile());
//            }
//            Log.d("**input",input);
            postRequestBuilder.addStringBody(input);
//            postRequestBuilder.addHeaders("X-Token", "3F284DE6FB872BE347D3B3E115A79");
            postRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                    if (jsonStr != null && jsonStr.contains("Invalid Token")) {
                        CommonMethods.hideProgressDialog();
                        CommonMethods.showToast("Invalid token logging out.", Toast.LENGTH_SHORT);
//                        ((MainActivity) CommonObjects.getContext()).clearLogOutClear();
                    } else {
                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                        Log.d("**serviceResponse", url + "->" + jsonStr);
                    }
                }

                @Override
                public void onError(ANError error) {
                    String err = Constants.Messages.NO_INTERNET_MESSAGE;
                    if (error.getErrorDetail() != null && error.getErrorDetail().equals("connectionError")) {
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    } else {
                        if (error.getErrorBody() != null) {
                            err = error.getErrorBody();
                        }
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    }
                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
        }
    }

    public void callForServerFilePost() {
        if (CommonMethods.isNetworkAvailable(CommonObjects.getContext())) {
            AndroidNetworking.initialize(CommonObjects.getContext());
            ANRequest.PostRequestBuilder postRequestBuilder = AndroidNetworking.post(API_BASE_URL + url);
            OkHttpClient client = new OkHttpClient()
                    .newBuilder()
                    .connectTimeout(30, TimeUnit.SECONDS)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .build();
            postRequestBuilder.setOkHttpClient(client);
            Log.d("Service_Response", url);
//            for (ParamString paramString : paramStrings) {
//                Log.d("Service_Response",paramString.getKey()+" "+paramString.getValue());
//                postRequestBuilder.addBodyParameter(paramString.getKey(), paramString.getValue());
//            }
            for (ParamFile paramFile : paramFiles) {
                Log.d("Service_Response", paramFile.getKey() + " " + paramFile.getFile().getAbsolutePath());
                postRequestBuilder.addFileBody(paramFile.getFile());
            }
//            postRequestBuilder.addHeaders("X-Token", "3F284DE6FB872BE347D3B3E115A79");
            postRequestBuilder.build().getAsString(new StringRequestListener() {
                @Override
                public void onResponse(String jsonStr) {
                    if (jsonStr != null && jsonStr.contains("Invalid Token")) {
                        CommonMethods.hideProgressDialog();
                        CommonMethods.showToast("Invalid token logging out.", Toast.LENGTH_SHORT);
//                        ((MainActivity) CommonObjects.getContext()).clearLogOutClear();
                    } else {
                        onServerResultNotifier.onServerResultNotifier(false, jsonStr);
                    }
                }

                @Override
                public void onError(ANError error) {
                    String err = Constants.Messages.NO_INTERNET_MESSAGE;
                    if (error.getErrorDetail() != null && error.getErrorDetail().equals("connectionError")) {
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    } else {
                        if (error.getErrorBody() != null) {
                            err = error.getErrorBody();
                        }
                        onServerResultNotifier.onServerResultNotifier(true, err);
                    }
                }
            });
        } else {
            onServerResultNotifier.onServerResultNotifier(true, NO_INTERNET);
        }
    }

}
