package com.tekfirst.e8glassofwater.business.models;


public class Glass {
    private int key;


    public Glass(Integer key)
    {
        this.key=key;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }
}
