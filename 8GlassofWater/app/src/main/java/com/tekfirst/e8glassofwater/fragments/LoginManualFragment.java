package com.tekfirst.e8glassofwater.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.utility.CommonMethods;


public class LoginManualFragment extends BaseFragment {
    @Override
    protected void initView() {
        mActivity.setTitle("Login");
        Button btnSignIn=mView.findViewById(R.id.btnSignIn);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(DashboardFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
            }
        });
    }

    @Override
    protected void loadData() {
    }

    public static LoginManualFragment newInstance() {
        LoginManualFragment fragment = new LoginManualFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_login_manual,null);
    }

    @Override
    public String getFragmentTag() {
        return LoginManualFragment.class.getSimpleName();
    }

}
