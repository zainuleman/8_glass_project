package com.tekfirst.e8glassofwater.business.handlers;

//
//import com.google.gson.Gson;
//import com.tekfirst.fms.business.models.AddComplainDropdownResponse;
//import com.tekfirst.fms.business.models.AddJobInput;
//import com.tekfirst.fms.business.models.AddTaskInput;
//import com.tekfirst.fms.business.models.AddTyreDropdownResponse;
//import com.tekfirst.fms.business.models.AddTyreResponse;
//import com.tekfirst.fms.business.models.AddVehicleInJobInput;
//import com.tekfirst.fms.business.models.AddWorksheetInput;
//import com.tekfirst.fms.business.models.ChangeTyreInput;
//import com.tekfirst.fms.business.models.ComplainInput;
//import com.tekfirst.fms.business.models.ComplainListResponse;
//import com.tekfirst.fms.business.models.CompleteJobInput;
//import com.tekfirst.fms.business.models.CompleteJobResponse;
//import com.tekfirst.fms.business.models.ConfigurationListResponse;
//import com.tekfirst.fms.business.models.Connection;
//import com.tekfirst.fms.business.models.CustomerDropdownResponse;
//import com.tekfirst.fms.business.models.CustomerListResponse;
//import com.tekfirst.fms.business.models.DeassignListResponse;
//import com.tekfirst.fms.business.models.DiscardInspectionInput;
//import com.tekfirst.fms.business.models.ForgotPasswordInput;
//import com.tekfirst.fms.business.models.GeneralObservationInput;
//import com.tekfirst.fms.business.models.GroupDropdownResponse;
//import com.tekfirst.fms.business.models.GroupListResponse;
//import com.tekfirst.fms.business.models.InspectionListResponse;
//import com.tekfirst.fms.business.models.InspectionNotAllowedDirectInput;
//import com.tekfirst.fms.business.models.InspectionNotAllowedJobBasedInput;
//import com.tekfirst.fms.business.models.InspectionSummaryResponse;
//import com.tekfirst.fms.business.models.InspectionSyncInput;
//import com.tekfirst.fms.business.models.InspectionSyncResponse;
//import com.tekfirst.fms.business.models.JobDetailResponse;
//import com.tekfirst.fms.business.models.JobListResponse;
//import com.tekfirst.fms.business.models.LogoutInput;
//import com.tekfirst.fms.business.models.LogoutResponse;
//import com.tekfirst.fms.business.models.LogsLogin;
//import com.tekfirst.fms.business.models.MoveVehicleInput;
//import com.tekfirst.fms.business.models.ParamFile;
//import com.tekfirst.fms.business.models.ParamString;
//import com.tekfirst.fms.business.models.ProfileUpdateInput;
//import com.tekfirst.fms.business.models.ReInspectionInput;
//import com.tekfirst.fms.business.models.RemovalCodeListResponse;
//import com.tekfirst.fms.business.models.RemoveTaskInput;
//import com.tekfirst.fms.business.models.RemoveTyreInput;
//import com.tekfirst.fms.business.models.RemoveWorksheetInput;
//import com.tekfirst.fms.business.models.ResetPasswordInput;
//import com.tekfirst.fms.business.models.SaveTaskQuestionsInput;
//import com.tekfirst.fms.business.models.ServiceProviderDropdownResponse;
//import com.tekfirst.fms.business.models.ServiceProviderListResponse;
//import com.tekfirst.fms.business.models.SignInInput;
//import com.tekfirst.fms.business.models.SignInResponse;
//import com.tekfirst.fms.business.models.SpecificDataResponse;
//import com.tekfirst.fms.business.models.SpecificDataWithIdResponse;
//import com.tekfirst.fms.business.models.StartAddInspectionDirectInput;
//import com.tekfirst.fms.business.models.StartAddInspectionJobBasedInput;
//import com.tekfirst.fms.business.models.SystemUpdatesResponse;
//import com.tekfirst.fms.business.models.TokenCIdPageInput;
//import com.tekfirst.fms.business.models.TokenIdInput;
//import com.tekfirst.fms.business.models.TokenInput;
//import com.tekfirst.fms.business.models.TokenInspectionIdInput;
//import com.tekfirst.fms.business.models.TokenLSDateInput;
//import com.tekfirst.fms.business.models.TokenLocalAndInspectionIdInput;
//import com.tekfirst.fms.business.models.TokenPageLSDateInput;
//import com.tekfirst.fms.business.models.TokenRNPageInput;
//import com.tekfirst.fms.business.models.TokenSpCIdInput;
//import com.tekfirst.fms.business.models.TokenSpIdInput;
//import com.tekfirst.fms.business.models.TokenVehicleHistoryInput;
//import com.tekfirst.fms.business.models.TyreInput;
//import com.tekfirst.fms.business.models.TyreInspectionInput;
//import com.tekfirst.fms.business.models.TyreSizeListResponse;
//import com.tekfirst.fms.business.models.UpdateTaskInput;
//import com.tekfirst.fms.business.models.UpdateWorksheetInput;
//import com.tekfirst.fms.business.models.UploadSignatureResponse;
//import com.tekfirst.fms.business.models.VehicleDetailResponse;
//import com.tekfirst.fms.business.models.VehicleInput;
//import com.tekfirst.fms.business.models.VehicleListResponse;
//import com.tekfirst.fms.business.models.VehicleSyncInput;
//import com.tekfirst.fms.business.models.VehicleSyncResponse;
//import com.tekfirst.fms.business.models.WorksheetListResponse;
//import com.tekfirst.fms.business.models.WorksheetNotAllowedInput;
//import com.tekfirst.fms.business.models.WorksheetSyncInput;
//import com.tekfirst.fms.business.models.WorksheetSyncResponse;
//import com.tekfirst.fms.business.models.YardListResponse;
//import com.tekfirst.fms.utility.CommonMethods;
//import com.tekfirst.fms.utility.CommonObjects;
//import com.tekfirst.fms.utility.Constants.Messages;


public class AppHandler {

//    public interface SignalListener {
//        public void onSignalReceive(String input);
//    }
//
//    public interface SignInListener {
//        public void onSignIn(SignInResponse signInResponse);
//
//        public void onError(String error);
//    }
//
//    public interface LogsListener {
//        public void onSignIn(SpecificDataResponse response);
//
//        public void onError(String error);
//    }
//
//    public interface ConnectionListener {
//        public void onResponse(Connection response);
//
//        public void onError(String error);
//    }
//
//    public interface SpecificListener {
//        public void onSpecificCall(SpecificDataResponse specificDataResponse);
//
//        public void onError(String error);
//    }
//
//    public interface SpecificWithIdListener {
//        public void onSpecificCallWithId(SpecificDataWithIdResponse specificDataWithIdResponse);
//
//        public void onError(String error);
//    }
//
//    public interface LogoutListener {
//        public void onLogout(LogoutResponse logoutResponse);
//
//        public void onError(String error);
//    }
//
//    public interface VehicleDetailListener {
//        public void onVehicleDetailListener(VehicleDetailResponse vehicleDetailResponse);
//
//        public void onError(String error);
//    }
//
//    public interface ServiceProviderDropdownListener {
//        public void onServiceProviderDropdownListener(ServiceProviderDropdownResponse serviceProviderDropdownResponse);
//
//        public void onError(String error);
//    }
//
//    public interface CustomerDropdownListener {
//        public void onCustomerDropdownListener(CustomerDropdownResponse customerDropdownResponse);
//
//        public void onError(String error);
//    }
//
//    public interface GroupDropdownListener {
//        public void onGroupDropdownListener(GroupDropdownResponse groupDropdownResponse);
//
//        public void onError(String error);
//    }
//
//    public interface VehicleListListener {
//        public void onVehicleListListener(VehicleListResponse vehicleListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface JobListListener {
//        public void onJobListListener(JobListResponse jobListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface JobDetailListener {
//        public void onJobDetailListener(JobDetailResponse jobDetailResponse);
//
//        public void onError(String error);
//    }
//
//    public interface ServiceProviderListListener {
//        public void onServiceProviderListListener(ServiceProviderListResponse serviceProviderListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface CustomerListListener {
//        public void onCustomerListListener(CustomerListResponse customerListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface GroupListListener {
//        public void onGroupListListener(GroupListResponse groupListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface ConfigurationListListener {
//        public void onConfigurationListListener(ConfigurationListResponse configurationListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface InspectionListListener {
//        public void onInspectionListListener(InspectionListResponse inspectionListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface YardListListener {
//        public void onYardListListener(YardListResponse inspectionListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface AddTyreDropdownListener {
//        public void onAddTyreDropdownListener(AddTyreDropdownResponse addTyreDropdownResponse);
//
//        public void onError(String error);
//    }
//
//    public interface InspectionSummaryListener {
//        public void onInspectionSummaryListener(InspectionSummaryResponse inspectionSummaryResponse);
//
//        public void onError(String error);
//    }
//
//    public interface WorksheetListListener {
//        public void onWorksheetListListener(WorksheetListResponse inspectionListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface AddComplainDropdownListener {
//        public void onAddComplainDropdownListener(AddComplainDropdownResponse addComplainDropdownResponse);
//
//        public void onError(String error);
//    }
//
//    public interface ComplainListListener {
//        public void onComplainListListener(ComplainListResponse complainListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface DeassignListListener {
//        public void onDeassignListListener(DeassignListResponse serviceProviderListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface UploadSignatureListener {
//        public void onUploadSignatureListener(UploadSignatureResponse uploadSignatureResponse);
//
//        public void onError(String error);
//    }
//
//    public interface InspectionSyncListener {
//        public void onInspectionSyncListener(InspectionSyncResponse inspectionSyncResponse);
//
//        public void onError(String error);
//    }
//
//    public interface VehicleSyncListener {
//        public void onVehicleSyncListener(VehicleSyncResponse vehicleSyncResponse);
//
//        public void onError(String error);
//    }
//
//    public interface WorksheetSyncListener {
//        public void onWorksheetSyncListener(WorksheetSyncResponse worksheetSyncResponse);
//
//        public void onError(String error);
//    }
//
//    public interface CompleteJobListener {
//        public void onCompleteJobListener(CompleteJobResponse completeJobResponse);
//
//        public void onError(String error);
//    }
//
//    public interface TyreSizeListListener {
//        public void onTyreSizeListListener(TyreSizeListResponse tyreSizeListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface RemovalCodeListListener {
//        public void onRemovalCodeListListener(RemovalCodeListResponse inspectionListResponse);
//
//        public void onError(String error);
//    }
//
//    public interface AddTyreListener {
//        public void onAddTyreListListener(AddTyreResponse addTyreResponse);
//
//        public void onError(String error);
//    }
//
//    public interface SystemUpdatesListener {
//        public void onSystemUpdates(SystemUpdatesResponse systemUpdatesResponse);
//
//        public void onError(String error);
//    }
//
//    //Call to fetch data
//    public static void signIn(final SignInInput signInInput, final SignInListener signInListener) {
//        new CallForServer("login", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SignInResponse signInResponse = new Gson().fromJson(response, SignInResponse.class);
//                        if (signInResponse.getCode().equals("200")) {
//                            callServerForLogs("login", "users", signInInput, signInResponse.getData().getUserId(), signInResponse.getStatus());
//                            signInListener.onSignIn(signInResponse);
//                        } else {
//                            signInListener.onError(signInResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        signInListener.onError(e.getMessage());
//                    }
//                } else {
//                    signInListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(signInInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void forgotPassword(ForgotPasswordInput forgotPasswordInput, final SpecificListener specificListener) {
//        new CallForServer("forgot", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        if (specificDataResponse.getCode().equals("200")) {
//                            specificListener.onSpecificCall(specificDataResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataResponse.getMessage());
//                            specificListener.onError(specificDataResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    specificListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(forgotPasswordInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void logout(final LogoutInput logoutInput, final LogoutListener logout) {
//        new CallForServer("logout", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        LogoutResponse logoutResponse = new Gson().fromJson(response, LogoutResponse.class);
//
//
//                        if (logoutResponse.getCode().equals("200")) {
//                            logout.onLogout(logoutResponse);
//                        } else {
//                            logout.onError(logoutResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        logout.onError(e.getMessage());
//                    }
//                } else {
//                    logout.onError(response);
//                }
//            }
//        }, new Gson().toJson(logoutInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void resetPassword(final ResetPasswordInput resetPasswordInput, final SpecificListener specificListener) {
//        new CallForServer("resetpass", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        callServerForLogs("resetpass", "users", resetPasswordInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataResponse.getStatus());
//
//                        if (specificDataResponse.getCode().equals("200")) {
//                            specificListener.onSpecificCall(specificDataResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataResponse.getMessage());
//                            specificListener.onError(specificDataResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    specificListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(resetPasswordInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void profileUpdate(final ProfileUpdateInput profileUpdateInput, final SpecificListener specificListener) {
//        new CallForServer("profile", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        callServerForLogs("profile", "users", profileUpdateInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataResponse.getStatus());
//
//                        if (specificDataResponse.getCode().equals("200")) {
//                            specificListener.onSpecificCall(specificDataResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataResponse.getMessage());
//                            specificListener.onError(specificDataResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    specificListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(profileUpdateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addEditVehicle(final VehicleInput vehicleInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("vehicles/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("vehicles/edit", "vehicles", vehicleInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(vehicleInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addEditTyre(final TyreInput tyreInput, final AddTyreListener addTyreListener) {
//        new CallForServer("vehicles/edittyre", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        AddTyreResponse addTyreResponse = new Gson().fromJson(response, AddTyreResponse.class);
//                        callServerForLogs("vehicles/edittyre", "vehicles", tyreInput, CommonObjects.getSignInResponse().getData().getUserId(), addTyreResponse.getStatus());
//
//                        if (addTyreResponse.getCode().equals("200")) {
//                            addTyreListener.onAddTyreListListener(addTyreResponse);
//                        } else {
//                            addTyreListener.onError(addTyreResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        addTyreListener.onError(e.getMessage());
//                    }
//                } else {
//                    addTyreListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tyreInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void saveVehicleStructure(final TokenIdInput tokenIdInput, final SpecificListener specificListener) {
//        new CallForServer("vehicles/savestructure", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        callServerForLogs("vehicles/savestructure", "vehicles", tokenIdInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataResponse.getStatus());
//
//                        if (specificDataResponse.getCode().equals("200")) {
//                            specificListener.onSpecificCall(specificDataResponse);
//                        } else {
//                            specificListener.onError(specificDataResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void vehicleDetail(final TokenIdInput tokenIdInput, final VehicleDetailListener vehicleDetailListener) {
//        new CallForServer("vehicles/detail", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        VehicleDetailResponse vehicleDetailResponse = new Gson().fromJson(response, VehicleDetailResponse.class);
//                        callServerForLogs("vehicles/detail", "vehicles", tokenIdInput, CommonObjects.getSignInResponse().getData().getUserId(), vehicleDetailResponse.getStatus());
//                        if (vehicleDetailResponse.getCode().equals("200")) {
//                            vehicleDetailListener.onVehicleDetailListener(vehicleDetailResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), vehicleDetailResponse.getStatus());
//                            vehicleDetailListener.onError(vehicleDetailResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        vehicleDetailListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    vehicleDetailListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void serviceProvidersDropdown(final TokenInput tokenInput, final ServiceProviderDropdownListener serviceProviderDropdownListener) {
//        new CallForServer("service_providers/dropdown", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        ServiceProviderDropdownResponse serviceProviderDropdownResponse = new Gson().fromJson(response, ServiceProviderDropdownResponse.class);
//                        callServerForLogs("service_providers/dropdown", "service_providers", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), serviceProviderDropdownResponse.getStatus());
//                        if (serviceProviderDropdownResponse.getCode().equals("200")) {
//                            serviceProviderDropdownListener.onServiceProviderDropdownListener(serviceProviderDropdownResponse);
//                        } else {
////                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), serviceProviderDropdownResponse.getLocalStatus());
//                            serviceProviderDropdownListener.onError(serviceProviderDropdownResponse.getStatus());
//                        }
//                    } catch (Exception e) {
////                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        serviceProviderDropdownListener.onError(e.getMessage());
//                    }
//                } else {
////                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    serviceProviderDropdownListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void customersDropdown(TokenSpIdInput tokenSpIdInput, final CustomerDropdownListener customerDropdownListener) {
//        new CallForServer("customers/dropdown", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        CustomerDropdownResponse customerDropdownResponse = new Gson().fromJson(response, CustomerDropdownResponse.class);
//                        if (customerDropdownResponse.getCode().equals("200")) {
//                            customerDropdownListener.onCustomerDropdownListener(customerDropdownResponse);
//                        } else {
////                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), customerDropdownResponse.getLocalStatus());
//                            customerDropdownListener.onError(customerDropdownResponse.getStatus());
//                        }
//                    } catch (Exception e) {
////                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        customerDropdownListener.onError(e.getMessage());
//                    }
//                } else {
////                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    customerDropdownListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenSpIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void groupsDropdown(TokenSpCIdInput tokenSpCIdInput, final GroupDropdownListener groupDropdownListener) {
//        new CallForServer("groups/dropdown", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        GroupDropdownResponse groupDropdownResponse = new Gson().fromJson(response, GroupDropdownResponse.class);
//                        if (groupDropdownResponse.getCode().equals("200")) {
//                            groupDropdownListener.onGroupDropdownListener(groupDropdownResponse);
//                        } else {
////                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), groupDropdownResponse.getLocalStatus());
//                            groupDropdownListener.onError(groupDropdownResponse.getStatus());
//                        }
//                    } catch (Exception e) {
////                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        groupDropdownListener.onError(e.getMessage());
//                    }
//                } else {
////                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    groupDropdownListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenSpCIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void vehicleList(final TokenPageLSDateInput tokenPageLSDateInput, final VehicleListListener vehicleListListener) {
//        new CallForServer("vehicles/vehicle_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        VehicleListResponse vehicleListResponse = new Gson().fromJson(response, VehicleListResponse.class);
//                        callServerForLogs("vehicles/vehicle_list", "vehicles", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), vehicleListResponse.getStatus());
//
//                        if (vehicleListResponse.getCode().equals("200")) {
//                            vehicleListListener.onVehicleListListener(vehicleListResponse);
//                        } else {
//                            vehicleListListener.onError(vehicleListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        vehicleListListener.onError(e.getMessage());
//                    }
//                } else {
//                    vehicleListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void searchVehicleByCustomer(final TokenCIdPageInput tokenCIdPageInput, final VehicleListListener vehicleListListener) {
//        new CallForServer("vehicles/search", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        VehicleListResponse vehicleListResponse = new Gson().fromJson(response, VehicleListResponse.class);
//                        callServerForLogs("vehicles/search", "vehicles", tokenCIdPageInput, CommonObjects.getSignInResponse().getData().getUserId(), vehicleListResponse.getStatus());
//
//                        if (vehicleListResponse.getCode().equals("200")) {
//                            vehicleListListener.onVehicleListListener(vehicleListResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), vehicleListResponse.getStatus());
//                            vehicleListListener.onError(vehicleListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        vehicleListListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    vehicleListListener.onError(response);
//
//                }
//            }
//        }, new Gson().toJson(tokenCIdPageInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void searchVehicleByRegNumber(TokenRNPageInput tokenRNPageInput, final VehicleListListener vehicleListListener) {
//        new CallForServer("vehicles/search", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        VehicleListResponse vehicleListResponse = new Gson().fromJson(response, VehicleListResponse.class);
//                        if (vehicleListResponse.getCode().equals("200")) {
//                            vehicleListListener.onVehicleListListener(vehicleListResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), vehicleListResponse.getStatus());
//                            vehicleListListener.onError(vehicleListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        vehicleListListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    vehicleListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenRNPageInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void jobList(final TokenPageLSDateInput tokenPageLSDateInput, final JobListListener jobListListener) {
//        Log.d("**joblistLSD", tokenPageLSDateInput.getLastSyncDate());
//        new CallForServer("jobs/job_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        JobListResponse jobListResponse = new Gson().fromJson(response, JobListResponse.class);
//                        callServerForLogs("jobs/job_list ", "jobs", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), jobListResponse.getStatus());
//                        int maxLogSize = 1000;
//                        for (int i = 0; i <= response.length() / maxLogSize; i++) {
//                            int start = i * maxLogSize;
//                            int end = (i + 1) * maxLogSize;
//                            end = end > response.length() ? response.length() : end;
//                            Log.v("**joblist", response.substring(start, end));
//                        }
//                        if (jobListResponse.getCode().equals("200")) {
//                            jobListListener.onJobListListener(jobListResponse);
//                        } else {
//                            jobListListener.onError(jobListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        jobListListener.onError(e.getMessage());
//                    }
//                } else {
//                    jobListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void jobDetail(TokenIdInput tokenIdInput, final JobDetailListener JobDetailListener) {
//        new CallForServer("jobs/detail", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        JobDetailResponse JobDetailResponse = new Gson().fromJson(response, JobDetailResponse.class);
//                        if (JobDetailResponse.getCode().equals("200")) {
//                            JobDetailListener.onJobDetailListener(JobDetailResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), JobDetailResponse.getStatus());
//                            JobDetailListener.onError(JobDetailResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        JobDetailListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    JobDetailListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void serviceProviderList(final TokenPageLSDateInput tokenPageLSDateInput, final ServiceProviderListListener serviceProviderListListener) {
//        new CallForServer("service_providers/sp_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        ServiceProviderListResponse serviceProviderListResponse = new Gson().fromJson(response, ServiceProviderListResponse.class);
//                        callServerForLogs("service_providers/sp_list", "service_providers", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), serviceProviderListResponse.getStatus());
//
//                        if (serviceProviderListResponse.getCode().equals("200")) {
//                            serviceProviderListListener.onServiceProviderListListener(serviceProviderListResponse);
//                        } else {
//                            serviceProviderListListener.onError(serviceProviderListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        serviceProviderListListener.onError(e.getMessage());
//                    }
//                } else {
//                    serviceProviderListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void customerList(final TokenPageLSDateInput tokenPageLSDateInput, final CustomerListListener customerListListener) {
//        new CallForServer("customers/customer_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        CustomerListResponse customerListResponse = new Gson().fromJson(response, CustomerListResponse.class);
//                        callServerForLogs("customers/customer_list", "customers", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), customerListResponse.getStatus());
//
//                        if (customerListResponse.getCode().equals("200")) {
//                            customerListListener.onCustomerListListener(customerListResponse);
//                        } else {
//                            customerListListener.onError(customerListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        customerListListener.onError(e.getMessage());
//                    }
//                } else {
//                    customerListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void groupList(final TokenPageLSDateInput tokenPageLSDateInput, final GroupListListener groupListListener) {
//        new CallForServer("groups/group_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        GroupListResponse groupListResponse = new Gson().fromJson(response, GroupListResponse.class);
//                        callServerForLogs("groups/group_list", "groups", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), groupListResponse.getStatus());
//
//                        if (groupListResponse.getCode().equals("200")) {
//                            groupListListener.onGroupListListener(groupListResponse);
//                        } else {
//                            groupListListener.onError(groupListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        groupListListener.onError(e.getMessage());
//                    }
//                } else {
//                    groupListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void configurationList(final TokenLSDateInput tokenLSDateInput, final ConfigurationListListener configurationListListener) {
//        new CallForServer("observations/config_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        ConfigurationListResponse configurationListResponse = new Gson().fromJson(response, ConfigurationListResponse.class);
//                        callServerForLogs("observations/config_list", "configs", tokenLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), configurationListResponse.getStatus());
//
//                        if (configurationListResponse.getCode().equals("200")) {
//                            configurationListListener.onConfigurationListListener(configurationListResponse);
//                        } else {
//                            configurationListListener.onError(configurationListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        configurationListListener.onError(e.getMessage());
//                    }
//                } else {
//                    configurationListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void configurationList(final TokenSpIdInput tokenSpIdInput, final ConfigurationListListener configurationListListener) {
//        new CallForServer("observations/config_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        ConfigurationListResponse configurationListResponse = new Gson().fromJson(response, ConfigurationListResponse.class);
//                        callServerForLogs("observations/config_list", "configs", tokenSpIdInput, CommonObjects.getSignInResponse().getData().getUserId(), configurationListResponse.getStatus());
//
//                        if (configurationListResponse.getCode().equals("200")) {
//                            configurationListListener.onConfigurationListListener(configurationListResponse);
//                        } else {
//                            configurationListListener.onError(configurationListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        configurationListListener.onError(e.getMessage());
//                    }
//                } else {
//                    configurationListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenSpIdInput)).callForServerPost();
//    }
//
//
//    //Call to fetch data
//    public static void addInspectionJobBased(final StartAddInspectionJobBasedInput startAddInspectionJobBasedInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/edit", "inspections", startAddInspectionJobBasedInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(startAddInspectionJobBasedInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addInspectionDirect(StartAddInspectionDirectInput startAddInspectionDirectInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(startAddInspectionDirectInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addTyreInspection(final TyreInspectionInput tyreInspectionInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/tyreinspection", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/tyreinspection", "inspections", tyreInspectionInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tyreInspectionInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addGeneralObservation(final GeneralObservationInput generalObservationInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/general_insp", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/general_insp", "inspections", generalObservationInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(generalObservationInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionList(final TokenPageLSDateInput tokenPageLSDateInput, final InspectionListListener inspectionListListener) {
//        new CallForServer("inspections/inspection_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        InspectionListResponse inspectionListResponse = new Gson().fromJson(response, InspectionListResponse.class);
//                        callServerForLogs("inspections/inspection_list", "inspections", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), inspectionListResponse.getStatus());
//
//                        if (inspectionListResponse.getCode().equals("200")) {
//                            inspectionListListener.onInspectionListListener(inspectionListResponse);
//                        } else {
//                            inspectionListListener.onError(inspectionListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        inspectionListListener.onError(e.getMessage());
//                    }
//                } else {
//                    inspectionListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionListByVehicle(final TokenVehicleHistoryInput tokenVehicleHistoryInput, final InspectionListListener inspectionListListener) {
//        new CallForServer("inspections/search", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        InspectionListResponse inspectionListResponse = new Gson().fromJson(response, InspectionListResponse.class);
//                        callServerForLogs("inspections/search", "inspections", tokenVehicleHistoryInput, CommonObjects.getSignInResponse().getData().getUserId(), inspectionListResponse.getStatus());
//
//                        if (inspectionListResponse.getCode().equals("200")) {
//                            inspectionListListener.onInspectionListListener(inspectionListResponse);
//                        } else {
//                            inspectionListListener.onError(inspectionListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        inspectionListListener.onError(e.getMessage());
//                    }
//                } else {
//                    inspectionListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenVehicleHistoryInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void yardList(final TokenPageLSDateInput tokenPageLSDateInput, final YardListListener yardListListener) {
//        new CallForServer("yards/yard_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        YardListResponse yardListResponse = new Gson().fromJson(response, YardListResponse.class);
//                        callServerForLogs("yards/yard_list", "yards", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), yardListResponse.getStatus());
//
//                        if (yardListResponse.getCode().equals("200")) {
//                            yardListListener.onYardListListener(yardListResponse);
//                        } else {
//                            yardListListener.onError(yardListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        yardListListener.onError(e.getMessage());
//                    }
//                } else {
//                    yardListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addTyreDropdown(final TokenInput tokenInput, final AddTyreDropdownListener addTyreDropdownListener) {
//        new CallForServer("vehicles/tyredropdowns", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        AddTyreDropdownResponse addTyreDropdownResponse = new Gson().fromJson(response, AddTyreDropdownResponse.class);
//                        callServerForLogs("vehicles/tyredropdowns", "vehicles", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), addTyreDropdownResponse.getStatus());
//
//                        if (addTyreDropdownResponse.getCode().equals("200")) {
//                            addTyreDropdownListener.onAddTyreDropdownListener(addTyreDropdownResponse);
//                        } else {
//                            addTyreDropdownListener.onError(addTyreDropdownResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        addTyreDropdownListener.onError(e.getMessage());
//                    }
//                } else {
//                    addTyreDropdownListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionSummary(final TokenInspectionIdInput tokenInspectionIdInput, final InspectionSummaryListener inspectionSummaryListener) {
//        new CallForServer("inspections/summary", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        InspectionSummaryResponse inspectionSummaryResponse = new Gson().fromJson(response, InspectionSummaryResponse.class);
//                        callServerForLogs("inspections/summary", "inspections", tokenInspectionIdInput, CommonObjects.getSignInResponse().getData().getUserId(), inspectionSummaryResponse.getStatus());
//
//                        if (inspectionSummaryResponse.getCode().equals("200")) {
//                            inspectionSummaryListener.onInspectionSummaryListener(inspectionSummaryResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), inspectionSummaryResponse.getStatus());
//                            inspectionSummaryListener.onError(inspectionSummaryResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        inspectionSummaryListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    inspectionSummaryListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInspectionIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionNotAllowedDirect(InspectionNotAllowedDirectInput inspectionNotAllowedDirectInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(inspectionNotAllowedDirectInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionNotAllowedJobBased(final InspectionNotAllowedJobBasedInput inspectionNotAllowedJobBasedInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/edit", "inspections", inspectionNotAllowedJobBasedInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(inspectionNotAllowedJobBasedInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void worksheetList(final TokenPageLSDateInput tokenPageLSDateInput, final WorksheetListListener worksheetListListener) {
//        Log.d("WorkList", new Gson().toJson(tokenPageLSDateInput));
//        new CallForServer("worksheets/worksheet_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        Log.d("WorkList", response);
//                        WorksheetListResponse worksheetListResponse = new Gson().fromJson(response, WorksheetListResponse.class);
//                        callServerForLogs("worksheets/worksheet_list", "worksheets", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), worksheetListResponse.getStatus());
//
//                        Log.d("WorkList", worksheetListResponse.getData().size() + "");
//                        if (worksheetListResponse.getCode().equals("200")) {
//                            worksheetListListener.onWorksheetListListener(worksheetListResponse);
//                        } else {
//                            worksheetListListener.onError(worksheetListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        worksheetListListener.onError(e.getMessage());
//                    }
//                } else {
//                    worksheetListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void inspectionComplete(final TokenLocalAndInspectionIdInput tokenLocalAndInspectionIdInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/complete", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/complete", "inspections", tokenLocalAndInspectionIdInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenLocalAndInspectionIdInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void updateWorksheetTask(final UpdateTaskInput updateTaskInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/updatetask", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/updatetask", "worksheets", updateTaskInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(updateTaskInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void changeTyre(final ChangeTyreInput changeTyreInput, final SpecificWithIdListener specificWithIdListener) {
//        Log.d("changeTyre_Input", new Gson().toJson(changeTyreInput));
//        new CallForServer("worksheets/change_tyre", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/change_tyre", "worksheets", changeTyreInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(changeTyreInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addJob(final AddJobInput addJobInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("jobs/add_job", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("jobs/add_job", "jobs", addJobInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(addJobInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void discardInspection(final DiscardInspectionInput discardInspectionInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("inspections/discard", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("inspections/discard", "inspections", discardInspectionInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
////                            CommonMethods.showMessage(CommonObjects.getContext(), specificDataWithIdResponse.getMessage());
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
////                        CommonMethods.showMessage(CommonObjects.getContext(), e.getMessage());
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(discardInspectionInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void completeJob(final CompleteJobInput completeJobInput, final CompleteJobListener completeJobListener) {
//        new CallForServer("jobs/complete", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        CompleteJobResponse completeJobResponse = new Gson().fromJson(response, CompleteJobResponse.class);
//                        callServerForLogs("jobs/complete", "jobs", completeJobInput, CommonObjects.getSignInResponse().getData().getUserId(), completeJobResponse.getStatus());
//
//                        if (completeJobResponse.getCode().equals("200")) {
//                            completeJobListener.onCompleteJobListener(completeJobResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            completeJobListener.onError(completeJobResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        completeJobListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    completeJobListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(completeJobInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addVehicleInJob(final AddVehicleInJobInput addVehicleInJobInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("jobs/add_vehicle", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("jobs/add_vehicle", "jobs", addVehicleInJobInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(addVehicleInJobInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void worksheetNotAllowed(final WorksheetNotAllowedInput worksheetNotAllowedInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/not_allowed", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/not_allowed", "worksheets", worksheetNotAllowedInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(worksheetNotAllowedInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void updateWorksheet(final UpdateWorksheetInput updateWorksheetInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/update_worksheet", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/update_worksheet", "worksheets", updateWorksheetInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(updateWorksheetInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void saveTaskQuestions(final SaveTaskQuestionsInput saveTaskQuestionsInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/save_task_questions", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/save_task_questions", "worksheets", saveTaskQuestionsInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(saveTaskQuestionsInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addComplainDropdown(final TokenInput tokenInput, final AddComplainDropdownListener addComplainDropdownListener) {
//        new CallForServer("complaints/issue_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        AddComplainDropdownResponse addComplainDropdownResponse = new Gson().fromJson(response, AddComplainDropdownResponse.class);
//                        callServerForLogs("complaints/issue_list", "complaints", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), addComplainDropdownResponse.getStatus());
//
//                        if (addComplainDropdownResponse.getCode().equals("200")) {
//                            addComplainDropdownListener.onAddComplainDropdownListener(addComplainDropdownResponse);
//                        } else {
//                            addComplainDropdownListener.onError(addComplainDropdownResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        addComplainDropdownListener.onError(e.getMessage());
//                    }
//                } else {
//                    addComplainDropdownListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void complainList(final TokenPageLSDateInput tokenPageLSDateInput, final ComplainListListener complainListListener) {
//        new CallForServer("complaints/complaint_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        ComplainListResponse complainListResponse = new Gson().fromJson(response, ComplainListResponse.class);
//                        callServerForLogs("complaints/complaint_list", "complaints", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), complainListResponse.getStatus());
//
//                        if (complainListResponse.getCode().equals("200")) {
//                            complainListListener.onComplainListListener(complainListResponse);
//                        } else {
//                            complainListListener.onError(complainListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        complainListListener.onError(e.getMessage());
//                    }
//                } else {
//                    complainListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addEditIncident(final ComplainInput complainInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("complaints/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("complaints/edit", "complaints", complainInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(complainInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void getProfile(final TokenInput tokenInput, final SignInListener signInListener) {
//        new CallForServer("user_info", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SignInResponse signInResponse = new Gson().fromJson(response, SignInResponse.class);
//                        callServerForLogs("user_info", "users", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), signInResponse.getStatus());
//
//                        if (signInResponse.getCode().equals("200")) {
//                            signInListener.onSignIn(signInResponse);
//                        } else {
//                            signInListener.onError(signInResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        signInListener.onError(e.getMessage());
//                    }
//                } else {
//                    signInListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void getProfileSync(final TokenInput tokenInput, final SignInListener signInListener) {
//        new CallForServer("user_info", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
////                    CommonMethods.hideProgressDialog();
//                    signInListener.onError(response);
//                } else if (!isError) {
//                    try {
//                        SignInResponse signInResponse = new Gson().fromJson(response, SignInResponse.class);
//                        callServerForLogs("user_info", "users", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), signInResponse.getStatus());
//
//                        if (signInResponse.getCode().equals("200")) {
//                            signInListener.onSignIn(signInResponse);
//                        } else {
//                            signInListener.onError(signInResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        signInListener.onError(e.getMessage());
//                    }
//                } else {
//                    signInListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void reInspection(final ReInspectionInput reInspectionInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/reinspection", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/reinspection", "worksheets", reInspectionInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(reInspectionInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void removeTyre(final RemoveTyreInput removeTyreInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("vehicles/removetyre", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("vehicles/removetyre", "vehicles", removeTyreInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(removeTyreInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void deassignList(final TokenPageLSDateInput tokenPageLSDateInput, final DeassignListListener deassignListListener) {
//        new CallForServer("deassign_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        DeassignListResponse deassignListResponse = new Gson().fromJson(response, DeassignListResponse.class);
//                        callServerForLogs("deassign_list", "configs", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), deassignListResponse.getStatus());
//
//                        if (deassignListResponse.getCode().equals("200")) {
//                            deassignListListener.onDeassignListListener(deassignListResponse);
//                        } else {
//                            deassignListListener.onError(deassignListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        deassignListListener.onError(e.getMessage());
//                    }
//                } else {
//                    deassignListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void uploadSignature(File file, final UploadSignatureListener uploadSignatureListener) {
//        ArrayList<ParamFile> paramFiles = new ArrayList<>();
//        ParamFile paramFile = new ParamFile("image", file);
//        paramFiles.add(paramFile);
//        new CallForServer("signatures/upload", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        UploadSignatureResponse uploadSignatureResponse = new Gson().fromJson(response, UploadSignatureResponse.class);
//                        if (uploadSignatureResponse.getCode().equals("200")) {
//                            uploadSignatureListener.onUploadSignatureListener(uploadSignatureResponse);
//                        } else {
//                            uploadSignatureListener.onError(uploadSignatureResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        uploadSignatureListener.onError(e.getMessage());
//                    }
//                } else {
//                    uploadSignatureListener.onError(response);
//                }
//            }
//        }, new ArrayList<ParamString>(), paramFiles).callForServerFilePost();
//    }
//
//    //Call to fetch data
//    public static void uploadSignatureSync(File file, final UploadSignatureListener uploadSignatureListener) {
//        ArrayList<ParamFile> paramFiles = new ArrayList<>();
//        ParamFile paramFile = new ParamFile("image", file);
//        paramFiles.add(paramFile);
//        new CallForServer("signatures/upload", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
////                    CommonMethods.hideProgressDialog();
//                    uploadSignatureListener.onError(response);
//                } else if (!isError) {
//                    try {
//                        UploadSignatureResponse uploadSignatureResponse = new Gson().fromJson(response, UploadSignatureResponse.class);
//                        if (uploadSignatureResponse.getCode().equals("200")) {
//                            uploadSignatureListener.onUploadSignatureListener(uploadSignatureResponse);
//                        } else {
//                            uploadSignatureListener.onError(uploadSignatureResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        uploadSignatureListener.onError(e.getMessage());
//                    }
//                } else {
//                    uploadSignatureListener.onError(response);
//                }
//            }
//        }, new ArrayList<ParamString>(), paramFiles).callForServerFilePost();
//    }
//
//    //Call to fetch data
//    public static void inspectionSync(final InspectionSyncInput inspectionSyncInput, final InspectionSyncListener inspectionSyncListener) {
//        Log.d("**jobs/inspection_sync", new Gson().toJson(inspectionSyncInput));
//        new CallForServer("jobs/inspection_sync", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
////                    CommonMethods.hideProgressDialog();
//                    inspectionSyncListener.onError(response);
//                } else if (!isError) {
//                    try {
//                        InspectionSyncResponse inspectionSyncResponse = new Gson().fromJson(response, InspectionSyncResponse.class);
//                        callServerForLogs("jobs/inspection_sync", "jobs", inspectionSyncInput, CommonObjects.getSignInResponse().getData().getUserId(), inspectionSyncResponse.getStatus());
//
//                        if (inspectionSyncResponse.getCode().equals("200")) {
//                            inspectionSyncListener.onInspectionSyncListener(inspectionSyncResponse);
//                        } else {
//                            inspectionSyncListener.onError(inspectionSyncResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        inspectionSyncListener.onError(e.getMessage());
//                    }
//                } else {
//                    inspectionSyncListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(inspectionSyncInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void vehicleSync(final VehicleSyncInput vehicleSyncInput, final VehicleSyncListener vehicleSyncListener) {
//        Log.d("**inputAxles", vehicleSyncInput.getParams().getNoOfAxles());
//        new CallForServer("vehicles/vehicle_sync", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
////                    CommonMethods.hideProgressDialog();
//                    vehicleSyncListener.onError(response);
//                } else if (!isError) {
//                    try {
//                        VehicleSyncResponse vehicleSyncResponse = new Gson().fromJson(response, VehicleSyncResponse.class);
//                        callServerForLogs("vehicles/vehicle_sync", "vehicles", vehicleSyncInput, CommonObjects.getSignInResponse().getData().getUserId(), vehicleSyncResponse.getStatus());
//
//                        if (vehicleSyncResponse.getCode().equals("200")) {
//                            vehicleSyncListener.onVehicleSyncListener(vehicleSyncResponse);
//                        } else {
//                            vehicleSyncListener.onError(vehicleSyncResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        vehicleSyncListener.onError(e.getMessage());
//                    }
//                } else {
//                    vehicleSyncListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(vehicleSyncInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addTask(final AddTaskInput addTaskInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/add_task", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/add_task", "worksheets", addTaskInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(addTaskInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void moveVehicle(final MoveVehicleInput moveVehicleInput, final SpecificListener specificListener) {
//        new CallForServer("jobs/move_vehicle", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        callServerForLogs("jobs/move_vehicle", "jobs", moveVehicleInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataResponse.getStatus());
//
//                        if (specificDataResponse.getCode().equals("200")) {
//                            specificListener.onSpecificCall(specificDataResponse);
//                        } else {
//                            specificListener.onError(specificDataResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(moveVehicleInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void worksheetSync(final WorksheetSyncInput worksheetSyncInput, final WorksheetSyncListener worksheetSyncListener) {
//        new CallForServer("worksheets/worksheet_sync", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    worksheetSyncListener.onError(response);
////                    CommonMethods.showMessage(CommonObjects.getContext(),response);
////                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        WorksheetSyncResponse worksheetSyncResponse = new Gson().fromJson(response, WorksheetSyncResponse.class);
//                        callServerForLogs("worksheets/worksheet_sync", "worksheets", worksheetSyncInput, CommonObjects.getSignInResponse().getData().getUserId(), worksheetSyncResponse.getStatus());
//
//                        if (worksheetSyncResponse.getCode().equals("200")) {
//                            worksheetSyncListener.onWorksheetSyncListener(worksheetSyncResponse);
//                        } else {
//                            worksheetSyncListener.onError(worksheetSyncResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        worksheetSyncListener.onError(e.getMessage());
//                    }
//                } else {
//                    worksheetSyncListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(worksheetSyncInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void tyreSizeList(final TokenPageLSDateInput tokenPageLSDateInput, final TyreSizeListListener tyreSizeListListener) {
//        new CallForServer("tyre_sizes/size_list", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        TyreSizeListResponse tyreSizeListResponse = new Gson().fromJson(response, TyreSizeListResponse.class);
//                        callServerForLogs("tyre_sizes/size_list", "vehicles", tokenPageLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), tyreSizeListResponse.getStatus());
//
//                        if (tyreSizeListResponse.getCode().equals("200")) {
//                            tyreSizeListListener.onTyreSizeListListener(tyreSizeListResponse);
//                        } else {
//                            tyreSizeListListener.onError(tyreSizeListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        tyreSizeListListener.onError(e.getMessage());
//                    }
//                } else {
//                    tyreSizeListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenPageLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void removalCodeList(final TokenInput tokenInput, final RemovalCodeListListener removalCodeListListener) {
//        new CallForServer("worksheets/removal_codes", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        RemovalCodeListResponse removalCodeListResponse = new Gson().fromJson(response, RemovalCodeListResponse.class);
//                        callServerForLogs("worksheets/removal_codes", "worksheets", tokenInput, CommonObjects.getSignInResponse().getData().getUserId(), removalCodeListResponse.getStatus());
//
//                        if (removalCodeListResponse.getCode().equals("200")) {
//                            removalCodeListListener.onRemovalCodeListListener(removalCodeListResponse);
//                        } else {
//                            removalCodeListListener.onError(removalCodeListResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        removalCodeListListener.onError(e.getMessage());
//                    }
//                } else {
//                    removalCodeListListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void addWorksheet(final AddWorksheetInput addWorksheetInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/edit", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/edit", "worksheets", addWorksheetInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(addWorksheetInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void removeTask(final RemoveTaskInput removeTaskInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/removetask", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/removetask", "worksheets", removeTaskInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(removeTaskInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void removeWorksheet(final RemoveWorksheetInput removeWorksheetInput, final SpecificWithIdListener specificWithIdListener) {
//        new CallForServer("worksheets/remove", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataWithIdResponse specificDataWithIdResponse = new Gson().fromJson(response, SpecificDataWithIdResponse.class);
//                        callServerForLogs("worksheets/remove", "worksheets", removeWorksheetInput, CommonObjects.getSignInResponse().getData().getUserId(), specificDataWithIdResponse.getStatus());
//
//                        if (specificDataWithIdResponse.getCode().equals("200")) {
//                            specificWithIdListener.onSpecificCallWithId(specificDataWithIdResponse);
//                        } else {
//                            specificWithIdListener.onError(specificDataWithIdResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        specificWithIdListener.onError(e.getMessage());
//                    }
//                } else {
//                    specificWithIdListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(removeWorksheetInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    public static void systemUpdates(final TokenLSDateInput tokenLSDateInput, final SystemUpdatesListener systemUpdatesListener) {
//        new CallForServer("systemupdates", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SystemUpdatesResponse systemUpdatesResponse = new Gson().fromJson(response, SystemUpdatesResponse.class);
//                        callServerForLogs("systemupdates", "configs", tokenLSDateInput, CommonObjects.getSignInResponse().getData().getUserId(), systemUpdatesResponse.getStatus());
//
//                        if (systemUpdatesResponse.getCode().equals("200")) {
//                            systemUpdatesListener.onSystemUpdates(systemUpdatesResponse);
//                        } else {
//                            CommonMethods.hideProgressDialog();
//                            systemUpdatesListener.onError(systemUpdatesResponse.getMessage());
//                        }
//                    } catch (Exception e) {
//                        CommonMethods.hideProgressDialog();
//                        systemUpdatesListener.onError(e.getMessage());
//                    }
//                } else {
//                    CommonMethods.hideProgressDialog();
//                    systemUpdatesListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(tokenLSDateInput)).callForServerPost();
//    }
//
//    //Call to fetch data
//    private static void logs(LogsLogin logsLogin, final LogsListener logsListener) {
//        new CallForServer("logs", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        SpecificDataResponse specificDataResponse = new Gson().fromJson(response, SpecificDataResponse.class);
//                        if (specificDataResponse.getCode().equals("200")) {
//                            logsListener.onSignIn(specificDataResponse);
//                        } else {
//                            logsListener.onError(specificDataResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        logsListener.onError(e.getMessage());
//                    }
//                } else {
//                    logsListener.onError(response);
//                }
//            }
//        }, new Gson().toJson(logsLogin)).callForServerPost();
//    }
//
//    public static void callServerForLogs(String name, String module, Object requestBoody, String userId, String status) {
//        LogsLogin logsLogin = new LogsLogin();
//        logsLogin.setName(name);
//        logsLogin.setModule(module);
//        logsLogin.setRequestBody(requestBoody);
//        logsLogin.setUserId(userId);
//        logsLogin.setResponse(status);
//        logs(logsLogin, new LogsListener() {
//            @Override
//            public void onSignIn(SpecificDataResponse response) {
//
//            }
//
//            @Override
//            public void onError(String error) {
//
//            }
//        });
//    }
//
//    //Call to fetch data
//    public static void checkConnection(final ConnectionListener connectionListener) {
//        new CallForServer("hasconnection", new CallForServer.OnServerResultNotifier() {
//            @Override
//            public void onServerResultNotifier(boolean isError, String response) {
//                if (response.equals(Messages.NO_INTERNET_MESSAGE)) {
//                    CommonMethods.showMessage(CommonObjects.getContext(), response);
//                    CommonMethods.hideProgressDialog();
//                } else if (!isError) {
//                    try {
//                        Connection specificDataResponse = new Gson().fromJson(response, Connection.class);
//                        if (specificDataResponse.getCode().equals("200")) {
//                            connectionListener.onResponse(specificDataResponse);
//                        } else {
//                            connectionListener.onError(specificDataResponse.getStatus());
//                        }
//                    } catch (Exception e) {
//                        connectionListener.onError(e.getMessage());
//                    }
//                } else {
//                    connectionListener.onError(response);
//                }
//            }
//        }).callForServerGet();
//    }
}

