package com.tekfirst.e8glassofwater.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

/**
 * A simple {@link Fragment} subclass.
 */

@SuppressLint("ValidFragment")
public abstract class BaseFragment extends Fragment {
    protected FragmentActivity mActivity;
    protected View mView;
    private ImageButton ibBack;

    public void setIbBack(ImageButton ibBack) {
        this.ibBack = ibBack;
    }

    public BaseFragment() {

    }

    abstract protected void initView();

    abstract protected void loadData();

    abstract public String getFragmentTag();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView = view;
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mActivity.getSupportFragmentManager().getBackStackEntryCount() == 0) {
                            return false;
                        }
                        if (ibBack == null) {
                            mActivity.getSupportFragmentManager().popBackStack();
                        } else {
                            ibBack.performClick();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        initView();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mActivity = getActivity();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
}
