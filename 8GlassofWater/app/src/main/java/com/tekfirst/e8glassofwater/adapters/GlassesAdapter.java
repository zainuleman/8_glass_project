package com.tekfirst.e8glassofwater.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.business.models.Glass;
import com.tekfirst.e8glassofwater.utility.CommonMethods;

import java.util.ArrayList;


public class GlassesAdapter extends RecyclerView.Adapter<GlassesAdapter.ViewHolder> {

    private ArrayList<Glass> listItems;
    private Context context;

    public class ViewHolder extends RecyclerView.ViewHolder {
        private View mView;


        private ViewHolder(View view) {
            super(view);
            mView = view;
//            llBackground = (LinearLayout) view.findViewById(R.id.llBackground);
        }
    }

    public GlassesAdapter(ArrayList<Glass> listItems) {
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(context==null)
            context = parent.getContext();
        View view = CommonMethods.createView(context, R.layout.list_item_glasses_list, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Glass glass = listItems.get(position);

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    public void addItem(Glass glass){
        listItems.add(glass);
        notifyDataSetChanged();
    }


}
