package com.tekfirst.e8glassofwater.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.utility.CommonMethods;


public class RewardFragment extends BaseFragment {
    @Override
    protected void initView() {
    }
    @Override
    protected void loadData() {
    }

    public static RewardFragment newInstance() {
        RewardFragment fragment = new RewardFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_rewards,null);
    }

    @Override
    public String getFragmentTag() {
        return RewardFragment.class.getSimpleName();
    }

}
