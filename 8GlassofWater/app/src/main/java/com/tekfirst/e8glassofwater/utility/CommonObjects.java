package com.tekfirst.e8glassofwater.utility;

import android.content.Context;




public class CommonObjects {
    private static Context context;

    private static String versionName;
    private static boolean isOffline;
    private static boolean isDoInspection;
    public static String getVersionName() {
        return versionName;
    }

    public static void setVersionName(String versionName) {
        CommonObjects.versionName = versionName;
    }

    public static boolean isIsDoInspection() {
        return isDoInspection;
    }

    public static void setIsDoInspection(boolean isDoInspection) {
        CommonObjects.isDoInspection = isDoInspection;
    }

    public static boolean isIsOffline() {
        return isOffline;
    }

    public static void setIsOffline(boolean isOffline) {
        CommonObjects.isOffline = isOffline;
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CommonObjects.context = context;
    }
}