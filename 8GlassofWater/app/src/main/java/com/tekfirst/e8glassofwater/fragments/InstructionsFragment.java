package com.tekfirst.e8glassofwater.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.utility.CommonMethods;


public class InstructionsFragment extends BaseFragment {
    @Override
    protected void initView() {
        mActivity.setTitle("Disclaimer");
        Button btnAgree=mView.findViewById(R.id.btnAgree);
        btnAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(DashboardFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
            }
        });
    }
    @Override
    protected void loadData() {
    }

    public static InstructionsFragment newInstance() {
        InstructionsFragment fragment = new InstructionsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_instructions,null);
    }

    @Override
    public String getFragmentTag() {
        return InstructionsFragment.class.getSimpleName();
    }

}
