package com.tekfirst.e8glassofwater.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.utility.CommonMethods;


public class RegisterFragment extends BaseFragment {
    @Override
    protected void initView() {
        mActivity.setTitle("Sign up");
        Button btnSignup=mView.findViewById(R.id.btnSignup);
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(RegisterDetailFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
            }
        });
    }
    @Override
    protected void loadData() {
    }

    public static RegisterFragment newInstance() {
        RegisterFragment fragment = new RegisterFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_register,null);
    }

    @Override
    public String getFragmentTag() {
        return RegisterFragment.class.getSimpleName();
    }

}
