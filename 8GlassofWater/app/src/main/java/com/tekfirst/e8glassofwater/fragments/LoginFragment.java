package com.tekfirst.e8glassofwater.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.utility.CommonMethods;


public class LoginFragment extends BaseFragment {
//    private static final String EMAIL = "email";
//    private LoginButton loginButton;
    private CallbackManager callbackManager;
    @Override
    protected void initView() {
        mActivity.setTitle("Login");
        Button btnSignIn=mView.findViewById(R.id.btnSignIn);
        Button btnRegister=mView.findViewById(R.id.btnRegister);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(LoginManualFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(RegisterFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
            }
        });
        facebookCallBackMethod();

    }

    private void facebookCallBackMethod() {
        callbackManager = CallbackManager.Factory.create();
//        loginButton = mView.findViewById(R.id.login_button);
//        loginButton.setPermissions(Arrays.asList(EMAIL));
//         If you are using in a fragment, call loginButton.setFragment(this);

////         Callback registration
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                // App code
//                Toast.makeText(mActivity,"Loged in as"+loginResult,Toast.LENGTH_SHORT).show();
//                CommonMethods.callFragment(DashboardFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
//            }
//            @Override
//            public void onCancel() {
//                // App code
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//            }
//        });
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        Toast.makeText(mActivity,"Loged in as"+loginResult,Toast.LENGTH_SHORT).show();
                        CommonMethods.callFragment(DashboardFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);
                    }
                    @Override
                    public void onCancel() {
                        // App code
                    }
                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        Log.d("**Credentials",data+"");
        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void loadData() {
    }

    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_login,null);
    }

    @Override
    public String getFragmentTag() {
        return LoginFragment.class.getSimpleName();
    }

}
