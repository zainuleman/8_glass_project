package com.tekfirst.e8glassofwater.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.tekfirst.e8glassofwater.R;
import com.tekfirst.e8glassofwater.adapters.GlassesAdapter;
import com.tekfirst.e8glassofwater.business.models.Glass;
import com.tekfirst.e8glassofwater.utility.CommonMethods;

import java.util.ArrayList;


public class DashboardFragment extends BaseFragment {

//    private long mLastClickTime = 0;
static final int REQUEST_IMAGE_CAPTURE = 1;
    private RecyclerView rvListItems;
    private ArrayList<Glass> glassesList;
    private GlassesAdapter adapter;
    private int glassesCount=0;
    @Override
    protected void initView() {
        mActivity.setTitle("Drink");
        rvListItems=mView.findViewById(R.id.rvListItems);
        rvListItems.setLayoutManager(new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false));
        glassesList=new ArrayList<>();
        adapter=new GlassesAdapter(getGlassesList());
        rvListItems.setAdapter(adapter);
        Button btnDrink=mView.findViewById(R.id.btnDrink);
        Button btnReward=mView.findViewById(R.id.btnReward);
        btnDrink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });
        btnReward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(RewardFragment.newInstance(), R.id.flFragmentContainer, 0, 0,0,0, mActivity, true);

            }
        });
    }
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            assert extras != null;
            glassesCount++;
            adapter.addItem(new Glass(glassesCount));
            Bitmap imageBitmap = (Bitmap) extras.get("data");
//            imageView.setImageBitmap(imageBitmap);
        }
    }
    private ArrayList<Glass> getGlassesList(){
        ArrayList<Glass> list=new ArrayList<>();
        list.add(new Glass(glassesCount));
        list.add(new Glass(glassesCount));
        list.add(new Glass(glassesCount));
//        list.add(new Glass(glassesCount));
//        list.add(new Glass(glassesCount));
//        list.add(new Glass(glassesCount));
//        list.add(new Glass(glassesCount));
//        list.add(new Glass(glassesCount));
        return list;
    }
    @Override
    protected void loadData() {
    }

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return CommonMethods.createView(mActivity, R.layout.fragment_dashboard,null);
    }

    @Override
    public String getFragmentTag() {
        return DashboardFragment.class.getSimpleName();
    }

}
